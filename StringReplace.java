package practice;
import java.util.Scanner;

public class StringReplace {

	public static void main(String[] args) {
	
        String str1, str2, targetStr, replacementStr;  
        char prevChar, newChar;  
          
         
        Scanner sc = new Scanner(System.in);  
    
        System.out.println("Enter 1st string :");  
        str1 = sc.nextLine();  
        System.out.println("Enter 2nd string :");  
        str2 = sc.nextLine();  
        System.out.println("Enter the target string for 1st string :");  
        targetStr = sc.nextLine();  
        System.out.println("Enter the replacement string for 1st string :");  
        replacementStr = sc.nextLine();  
        System.out.println("Enter character which you want to replace in 2nd string :");  
        prevChar = sc.next().charAt(0);  
        System.out.println("Enter character which take place of :"+prevChar+" in 2nd string :");  
        newChar = sc.next().charAt(0);  
        sc.close();  
        String replaceStr1 = str2.replace(prevChar, newChar);//replaces all occurrences of prevChar to newChar   
        System.out.println("String after replacing "+prevChar+" with "+newChar+" is: "+replaceStr1);  
        String replaceStr2 = str1.replace(targetStr, replacementStr);//replaces all occurrences of targetStr with replacementStr   
        System.out.println("String after replacing "+targetStr+" with "+replacementStr+" is: "+replaceStr2);  

	}

}
