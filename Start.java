package practice;

public class Start {

	public static void main(String[] args) {
		String str="arun";
		System.out.println(str.startsWith("a"));
		System.out.println(str.startsWith("r"));
		System.out.println(str.startsWith("u"));
		System.out.println(str.startsWith("n"));
		char ch[]=str.toCharArray();
		for(int i=0;i<ch.length;i++) {
           System.out.println(ch[i]);			
		}
		System.out.println(ch);
		String s=str.toUpperCase();
		System.out.println(s);
		System.out.println(s.toLowerCase());
		String s1="  hello string   ";  
		System.out.println(s1+"javatpoint");//without trim()  
		System.out.println(s1.trim()+"javatpoint");//with trim()  
		int value=30;  
		String s7=String.valueOf(value);  
		System.out.println(s7+"10");//concatenating string with 10  

	}

}
